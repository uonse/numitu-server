package org.uonse.numitu.server.tutorial.plan.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;
import org.uonse.numitu.server.tutorial.plan.service.TutorialPlanService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
@RestController
@Transactional
public class TutorialPlanController {

    TutorialPlanService tutorialPlanService;
    TutorialPlanDtoTransformer tutorialPlanDtoTransformer;

    @Autowired
    public TutorialPlanController(TutorialPlanService tutorialPlanService, TutorialPlanDtoTransformer tutorialPlanDtoTransformer) {
        this.tutorialPlanService = tutorialPlanService;
        this.tutorialPlanDtoTransformer = tutorialPlanDtoTransformer;
    }

    @RequestMapping(value = "/tutorialplans", method = RequestMethod.GET)
    public List<TutorialPlanDto> list() {
        return tutorialPlanService.list().stream().map(tutorialPlanDtoTransformer::transform).collect(Collectors.toList());
    }

    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}", method = RequestMethod.GET)
    public TutorialPlanDto get(@PathVariable Long tutorialPlanId) {
        TutorialPlan plan = tutorialPlanService.get(tutorialPlanId);
        return tutorialPlanDtoTransformer.transform(plan);
    }
}
