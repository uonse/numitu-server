package org.uonse.numitu.server.tutorial.session.rest;

import org.springframework.stereotype.Component;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;
import org.uonse.numitu.server.util.Transform;

/**
 * Created by mhudson
 *
 * @since 18/08/15.
 */
@Component
public class TutorialSessionDtoTransformer implements Transform<TutorialSession, TutorialSessionDto> {
    @Override
    public TutorialSessionDto transform(TutorialSession tutorialSession) {
        return new TutorialSessionDto(tutorialSession.getId(), tutorialSession.getTutorialPlan().getId());
    }
}
