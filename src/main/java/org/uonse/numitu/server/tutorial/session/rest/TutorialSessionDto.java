package org.uonse.numitu.server.tutorial.session.rest;

import org.uonse.numitu.server.AbstractEntityDto;

import javax.validation.constraints.NotNull;

/**
 * Created by mhudson
 *
 * @since 18/08/15.
 */
public class TutorialSessionDto extends AbstractEntityDto {
    @NotNull
    private Long tutorialPlanId;

    public TutorialSessionDto(Long tutorialSessionId, Long tutorialPlanId) {
        this.id = tutorialSessionId;
        this.tutorialPlanId = tutorialPlanId;
    }

    public Long getTutorialPlanId() {
        return tutorialPlanId;
    }

    public void setTutorialPlanId(Long tutorialPlanId) {
        this.tutorialPlanId = tutorialPlanId;
    }
}
