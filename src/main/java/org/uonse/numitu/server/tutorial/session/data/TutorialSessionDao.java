package org.uonse.numitu.server.tutorial.session.data;

import org.uonse.numitu.server.NumituRepository;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;


/**
 * Created by mhudson
 *
 * @since 11/08/15.
 */
public interface TutorialSessionDao extends NumituRepository<TutorialSession> {
}
