package org.uonse.numitu.server.tutorial.plan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uonse.numitu.server.tutorial.plan.data.TutorialPlanDao;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;

import java.util.List;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
@Service
public class TutorialPlanServiceImpl implements TutorialPlanService {

    private TutorialPlanDao tutorialPlanDao;

    @Autowired
    public TutorialPlanServiceImpl(TutorialPlanDao tutorialPlanDao) {
        this.tutorialPlanDao = tutorialPlanDao;
    }

    public List<TutorialPlan> list() {
        return tutorialPlanDao.findAll();
    }

    @Override
    public TutorialPlan get(Long tutorialPlanId) {
        return tutorialPlanDao.findOne(tutorialPlanId);
    }
}
