package org.uonse.numitu.server.tutorial.session.domain;


import org.hibernate.annotations.Where;
import org.uonse.numitu.server.AbstractEntity;
import org.uonse.numitu.server.activity.instance.domain.ActivityInstance;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mhudson on 11/08/15.
 */
@Entity
@Table(name = "tutorials")
@Where(clause = "tutorial_plan_id is not null")
public class TutorialSession extends AbstractEntity {
    @JoinColumn(name = "tutorial_plan_id")
    @OneToOne
    private TutorialPlan tutorialPlan;

    @JoinColumn(name = "tutorial_id")
    @OneToMany
    private List<ActivityInstance> activities;

    public TutorialPlan getTutorialPlan() {
        return tutorialPlan;
    }

    public void setTutorialPlan(TutorialPlan tutorialPlan) {
        this.tutorialPlan = tutorialPlan;
    }

    public List<ActivityInstance> getActivities() {
        return activities;
    }

    public void setActivities(List<ActivityInstance> activities) {
        this.activities = activities;
    }
}
