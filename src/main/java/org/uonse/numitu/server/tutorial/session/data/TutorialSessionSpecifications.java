package org.uonse.numitu.server.tutorial.session.data;

import org.springframework.data.jpa.domain.Specification;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan_;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession_;

/**
 * Created by mhudson
 *
 * @since 18/08/15.
 */
public class TutorialSessionSpecifications {
    public static Specification<TutorialSession> forTutorialPlan(TutorialPlan tutorialPlan) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(TutorialSession_.tutorialPlan), tutorialPlan);
    }

    public static Specification<TutorialSession> forTutorialPlan(Long tutorialPlanId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(TutorialSession_.tutorialPlan).get(TutorialPlan_.id), tutorialPlanId);
    }
}
