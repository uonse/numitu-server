package org.uonse.numitu.server.tutorial.plan.data;


import org.uonse.numitu.server.NumituRepository;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;

/**
 * Created by mhudson
 *
 * @since 11/08/15.
 */
public interface TutorialPlanDao extends NumituRepository<TutorialPlan>{
}
