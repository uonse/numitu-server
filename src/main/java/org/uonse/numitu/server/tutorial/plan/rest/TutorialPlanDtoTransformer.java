package org.uonse.numitu.server.tutorial.plan.rest;

import org.springframework.stereotype.Component;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;
import org.uonse.numitu.server.util.Transform;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
@Component
public class TutorialPlanDtoTransformer implements Transform<TutorialPlan, TutorialPlanDto> {
    public TutorialPlanDto transform(TutorialPlan tutorialPlan) {
        return new TutorialPlanDto(tutorialPlan.getId(), tutorialPlan.getTutorialSessionCount());
    }
}
