package org.uonse.numitu.server.tutorial.session.service;

import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;

import java.util.List;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
public interface TutorialSessionService {
    List<TutorialSession> listForTutorialPlan(TutorialPlan tutorialPlan);
    List<TutorialSession> listForTutorialPlan(Long tutorialPlanId);
    TutorialSession get(Long tutorialSessionId);
}
