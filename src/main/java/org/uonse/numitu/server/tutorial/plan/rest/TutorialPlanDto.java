package org.uonse.numitu.server.tutorial.plan.rest;

import org.uonse.numitu.server.AbstractEntityDto;

import javax.validation.constraints.NotNull;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
public class TutorialPlanDto extends AbstractEntityDto {
    @NotNull
    private Integer tutorialSessionCount;

    public TutorialPlanDto() {}

    public TutorialPlanDto(Long tutorialPlanId, Integer tutorialSessionCount) {
        this.id = tutorialPlanId;
        this.tutorialSessionCount = tutorialSessionCount;
    }

    public Integer getTutorialSessionCount() {
        return tutorialSessionCount;
    }

    public void setTutorialSessionCount(Integer tutorialSessionCount) {
        this.tutorialSessionCount = tutorialSessionCount;
    }
}
