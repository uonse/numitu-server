package org.uonse.numitu.server.tutorial.plan.service;

import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;

import java.util.List;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
public interface TutorialPlanService {
    List<TutorialPlan> list();
    TutorialPlan get(Long tutorialPlanId);
}
