package org.uonse.numitu.server.tutorial.session.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;
import org.uonse.numitu.server.tutorial.session.data.TutorialSessionDao;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;

import java.util.List;

import static org.uonse.numitu.server.tutorial.session.data.TutorialSessionSpecifications.*;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
@Service
public class TutorialSessionServiceImpl implements TutorialSessionService{

    TutorialSessionDao tutorialSessionDao;

    @Autowired
    public TutorialSessionServiceImpl(TutorialSessionDao tutorialSessionDao) {
        this.tutorialSessionDao = tutorialSessionDao;
    }

    @Override
    public List<TutorialSession> listForTutorialPlan(TutorialPlan tutorialPlan) {
        return tutorialSessionDao.findAll(forTutorialPlan(tutorialPlan));
    }

    @Override
    public List<TutorialSession> listForTutorialPlan(Long tutorialPlanId) {
        //Todo: Validate that the tutorial plan exists
        return tutorialSessionDao.findAll(forTutorialPlan(tutorialPlanId));
    }

    @Override
    public TutorialSession get(Long tutorialSessionId) {
        return tutorialSessionDao.findOne(tutorialSessionId);
    }
}
