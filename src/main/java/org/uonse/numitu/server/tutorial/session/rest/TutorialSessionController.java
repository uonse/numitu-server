package org.uonse.numitu.server.tutorial.session.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.uonse.numitu.server.tutorial.session.service.TutorialSessionService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
@RestController
@Transactional
public class TutorialSessionController {

    private TutorialSessionService tutorialSessionService;
    private TutorialSessionDtoTransformer tutorialSessionDtoTransformer;

    @Autowired
    public TutorialSessionController(TutorialSessionService tutorialSessionService, TutorialSessionDtoTransformer tutorialSessionDtoTransformer) {
        this.tutorialSessionService = tutorialSessionService;
        this.tutorialSessionDtoTransformer = tutorialSessionDtoTransformer;
    }

    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}/tutorialsessions", method = RequestMethod.GET)
    public List<TutorialSessionDto> listForTutorialPlan(@PathVariable Long tutorialPlanId) {
        return tutorialSessionService.listForTutorialPlan(tutorialPlanId).stream().map(tutorialSessionDtoTransformer::transform).collect(Collectors.toList());
    }

    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}/tutorialsessions/{tutorialSessionId}", method = RequestMethod.GET)
    public TutorialSessionDto get(@PathVariable Long tutorialSessionId) {
        return tutorialSessionDtoTransformer.transform(tutorialSessionService.get(tutorialSessionId));
    }
}
