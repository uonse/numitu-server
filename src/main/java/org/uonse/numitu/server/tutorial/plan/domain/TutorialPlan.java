package org.uonse.numitu.server.tutorial.plan.domain;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Where;
import org.uonse.numitu.server.AbstractEntity;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by mhudson on 11/08/15.
 */
@Entity
@Table(name = "tutorials")
@Where(clause = "tutorial_plan_id is null")
public class TutorialPlan extends AbstractEntity {

    @Formula("(SELECT COUNT(*) FROM tutorials t WHERE t.tutorial_plan_id = id)")
    private Integer tutorialSessionCount;

    @JoinColumn(name = "tutorial_id")
    @OneToMany
    private List<ActivityPlan> activityPlans;

    public TutorialPlan(){}

    public TutorialPlan(Long id, Integer tutorialSessionCount) {
        this.id = id;
        this.tutorialSessionCount = tutorialSessionCount;
    }

    public Integer getTutorialSessionCount() {
        return tutorialSessionCount;
    }

    public List<ActivityPlan> getActivities() {
        return activityPlans;
    }

    public void setActivities(List<ActivityPlan> activities) {
        this.activityPlans = activities;
    }
}
