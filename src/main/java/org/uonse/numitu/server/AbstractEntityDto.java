package org.uonse.numitu.server;

import javax.validation.constraints.NotNull;

/**
 * Created by mhudson
 *
 * @since 13/08/15.
 */
public class AbstractEntityDto {
    @NotNull
    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
