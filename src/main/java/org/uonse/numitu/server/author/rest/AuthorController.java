package org.uonse.numitu.server.author.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.uonse.numitu.server.author.domain.Author;
import org.uonse.numitu.server.author.service.AuthorService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Michael Hudson
 * @since 14/05/2015.
 */
@RestController
@Transactional
public class AuthorController {

    private AuthorService authorService;
    private AuthorDtoTransformer authorDtoTransformer;

    @Autowired
    public AuthorController(AuthorService authorService, AuthorDtoTransformer authorDtoTransformer) {
        this.authorService = authorService;
        this.authorDtoTransformer = authorDtoTransformer;
    }

    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public List<AuthorDto> listAuthors() {
        List<AuthorDto> authorDtos = new ArrayList<AuthorDto>();
        for (Author author : authorService.listAuthors()) {
            authorDtos.add(authorDtoTransformer.transform(author));
        }
        return authorDtos;
    }

    @RequestMapping(value = "/authors/fragment/{fragment}", method = RequestMethod.GET)
    public List<AuthorDto> listAuthorsWithNameFragment(@PathVariable String fragment) {
        return authorService.listAuthorsWithNameFragment(fragment).stream().map(authorDtoTransformer::transform).collect(Collectors.toList());
    }
}
