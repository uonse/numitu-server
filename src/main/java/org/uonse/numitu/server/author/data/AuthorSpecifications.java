package org.uonse.numitu.server.author.data;

import org.springframework.data.jpa.domain.Specification;
import org.uonse.numitu.server.author.domain.Author;
import org.uonse.numitu.server.author.domain.Author_;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by mhudson
 *
 * @since 15/08/15.
 */
public class AuthorSpecifications {

    public static Specification<Author> hasNameFragment(final String fragment) {
        return new Specification<Author>() {
            public Predicate toPredicate(Root<Author> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(root.get(Author_.fullName), "%"+fragment+"%");
            }
        };
    }
}
