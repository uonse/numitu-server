package org.uonse.numitu.server.author.service;

import org.uonse.numitu.server.author.domain.Author;

import java.util.List;

/**
 * Created by Michael Hudson
 * @since 14/05/2015.
 */
public interface AuthorService {
    public List<Author> listAuthors();
    public List<Author> listAuthorsWithNameFragment(String fragment);
}
