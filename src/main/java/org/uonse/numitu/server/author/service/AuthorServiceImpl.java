package org.uonse.numitu.server.author.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uonse.numitu.server.author.data.AuthorDao;
import org.uonse.numitu.server.author.domain.Author;

import java.util.List;

import static org.uonse.numitu.server.author.data.AuthorSpecifications.*;

/**
 * Created by Michael Hudson
 * @since 14/05/2015.
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    private AuthorDao authorDao;

    @Autowired
    public AuthorServiceImpl(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    public List<Author> listAuthors() {
        return authorDao.findAll();
    }

    public List<Author> listAuthorsWithNameFragment(String fragment) {
        return authorDao.findAll(hasNameFragment(fragment));
    }
}
