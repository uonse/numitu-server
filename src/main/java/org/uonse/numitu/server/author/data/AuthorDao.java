package org.uonse.numitu.server.author.data;

import org.uonse.numitu.server.NumituRepository;
import org.uonse.numitu.server.author.domain.Author;


/**
 * Created by Michael Hudson
 *
 * @since 16/05/2015.
 */
public interface AuthorDao extends NumituRepository<Author>{
}
