package org.uonse.numitu.server.activity.instance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uonse.numitu.server.activity.instance.data.ActivityInstanceDao;
import org.uonse.numitu.server.activity.instance.domain.ActivityInstance;

import java.util.List;

import static org.uonse.numitu.server.activity.instance.data.ActivityInstanceSpecifications.*;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
@Service
public class ActivityInstanceServiceImpl implements ActivityInstanceService {

    private ActivityInstanceDao activityInstanceDao;

    @Autowired
    public ActivityInstanceServiceImpl(ActivityInstanceDao activityInstanceDao) {
        this.activityInstanceDao = activityInstanceDao;
    }

    @Override
    public ActivityInstance get(Long activityInstanceId) {
        return activityInstanceDao.findOne(activityInstanceId);
    }

    @Override
    public List<ActivityInstance> listForTutorialSession(Long tutorialSessionId) {
        return activityInstanceDao.findAll(forTutorialSession(tutorialSessionId));
    }
}
