package org.uonse.numitu.server.activity.instance.rest;

import org.springframework.stereotype.Component;
import org.uonse.numitu.server.activity.instance.domain.ActivityInstance;
import org.uonse.numitu.server.util.Transform;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
@Component
public class ActivityInstanceDtoTransformer implements Transform<ActivityInstance, ActivityInstanceDto>{
    @Override
    public ActivityInstanceDto transform(ActivityInstance activityInstance) {
        return new ActivityInstanceDto(activityInstance.getId(), activityInstance.getTutorialSession().getId());
    }
}
