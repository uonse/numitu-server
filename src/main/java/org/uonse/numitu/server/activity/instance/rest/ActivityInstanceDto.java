package org.uonse.numitu.server.activity.instance.rest;

import org.uonse.numitu.server.AbstractEntityDto;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
public class ActivityInstanceDto extends AbstractEntityDto {
    private Long tutorialSessionId;

    public ActivityInstanceDto(Long id, Long tutorialSessionId) {
        this.id = id;
        this.tutorialSessionId = tutorialSessionId;
    }

    public Long getTutorialSessionId() {
        return tutorialSessionId;
    }

    public void setTutorialSessionId(Long tutorialSessionId) {
        this.tutorialSessionId = tutorialSessionId;
    }
}
