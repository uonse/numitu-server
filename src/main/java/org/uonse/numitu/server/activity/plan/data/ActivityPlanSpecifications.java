package org.uonse.numitu.server.activity.plan.data;

import org.springframework.data.jpa.domain.Specification;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan_;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan_;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
public class ActivityPlanSpecifications {
    public static Specification<ActivityPlan> forTutorialPlan(Long tutorialPlanId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(ActivityPlan_.tutorialPlan).get(TutorialPlan_.id), tutorialPlanId);
    }

    public static Specification<ActivityPlan> forTutorialSession(TutorialSession tutorialSession) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(ActivityPlan_.tutorialPlan), tutorialSession.getTutorialPlan());
    }
}
