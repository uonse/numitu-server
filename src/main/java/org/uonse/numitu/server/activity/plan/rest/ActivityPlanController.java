package org.uonse.numitu.server.activity.plan.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.uonse.numitu.server.activity.plan.service.ActivityPlanService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Michael Hudson
 *
 * @since 26/08/2015.
 */
@RestController
public class ActivityPlanController {

    private ActivityPlanService activityPlanService;
    private ActivityPlanDtoTransformer activityPlanDtoTransformer;

    @Autowired
    public ActivityPlanController(ActivityPlanService activityPlanService, ActivityPlanDtoTransformer activityPlanDtoTransformer) {
        this.activityPlanService = activityPlanService;
        this.activityPlanDtoTransformer = activityPlanDtoTransformer;
    }

    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}/activities", method = RequestMethod.GET)
    public List<ActivityPlanDto> listForTutorialPlan(@PathVariable Long tutorialPlanId) {
        return activityPlanService.listForTutorialPlan(tutorialPlanId).stream().map(activityPlanDtoTransformer::transform).collect(Collectors.toList());
    }

    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}/activities/{activityPlanId}", method = RequestMethod.GET)
    public ActivityPlanDto getForTutorialPlan(@PathVariable Long tutorialPlanId, @PathVariable Long activityPlanId) {
        return activityPlanDtoTransformer.transform(activityPlanService.get(activityPlanId));
    }
}
