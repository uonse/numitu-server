package org.uonse.numitu.server.activity.instance.domain;

import org.hibernate.annotations.Where;
import org.uonse.numitu.server.AbstractEntity;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
@Entity
@Table(name = "activities")
@Where(clause = "activity_plan_id is not null")
public class ActivityInstance extends AbstractEntity{
    @OneToOne
    @JoinColumn(name = "tutorial_id")
    private TutorialSession tutorialSession;
    @OneToOne
    @JoinColumn(name = "activity_plan_id")
    private ActivityPlan activityPlan;

    public TutorialSession getTutorialSession() {
        return tutorialSession;
    }

    public void setTutorialSession(TutorialSession tutorialSession) {
        this.tutorialSession = tutorialSession;
    }

    public ActivityPlan getActivityPlan() {
        return activityPlan;
    }

    public void setActivityPlan(ActivityPlan activityPlan) {
        this.activityPlan = activityPlan;
    }
}
