package org.uonse.numitu.server.activity.instance.service;

import org.uonse.numitu.server.activity.instance.domain.ActivityInstance;

import java.util.List;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
public interface ActivityInstanceService {
    ActivityInstance get(Long activityInstanceId);
    List<ActivityInstance> listForTutorialSession(Long tutorialSessionId);
}
