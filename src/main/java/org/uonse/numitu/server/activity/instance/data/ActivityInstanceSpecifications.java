package org.uonse.numitu.server.activity.instance.data;

import org.springframework.data.jpa.domain.Specification;
import org.uonse.numitu.server.activity.instance.domain.ActivityInstance;
import org.uonse.numitu.server.activity.instance.domain.ActivityInstance_;
import org.uonse.numitu.server.tutorial.session.domain.TutorialSession_;


/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
public class ActivityInstanceSpecifications {
    public static Specification<ActivityInstance> forTutorialSession(final Long tutorialSessionId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get(ActivityInstance_.tutorialSession).get(TutorialSession_.id), tutorialSessionId);
    }
}
