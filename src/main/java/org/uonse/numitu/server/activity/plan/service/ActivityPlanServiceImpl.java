package org.uonse.numitu.server.activity.plan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uonse.numitu.server.activity.plan.data.ActivityPlanDao;
import org.uonse.numitu.server.activity.plan.data.ActivityPlanSpecifications;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;

import java.util.List;

/**
 * Created by mhudson
 *
 * @since 14/08/15.
 */
@Service
public class ActivityPlanServiceImpl implements ActivityPlanService {

    private ActivityPlanDao activityPlanDao;

    @Autowired
    public ActivityPlanServiceImpl(ActivityPlanDao activityPlanDao) {
        this.activityPlanDao = activityPlanDao;
    }

    public ActivityPlan get(Long activityPlanId) {
        return activityPlanDao.findOne(activityPlanId);
    }

    public List<ActivityPlan> listForTutorialPlan(Long tutorialPlanId) {
        return activityPlanDao.findAll(ActivityPlanSpecifications.forTutorialPlan(tutorialPlanId));
    }
}
