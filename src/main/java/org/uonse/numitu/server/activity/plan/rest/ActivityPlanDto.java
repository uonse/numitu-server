package org.uonse.numitu.server.activity.plan.rest;

import org.uonse.numitu.server.AbstractEntityDto;

/**
 * Created by Michael Hudson
 *
 * @since 26/08/2015.
 */
public class ActivityPlanDto extends AbstractEntityDto {
    private Long tutorialPlanId;

    public ActivityPlanDto(Long id, Long tutorialPlanId) {
        this.id = id;
        this.tutorialPlanId = tutorialPlanId;
    }

    public Long getTutorialPlanId() {
        return tutorialPlanId;
    }

    public void setTutorialPlanId(Long tutorialPlanId) {
        this.tutorialPlanId = tutorialPlanId;
    }
}
