package org.uonse.numitu.server.activity.plan.service;

import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;

import java.util.List;

/**
 * Created by mhudson on 11/08/15.
 */
public interface ActivityPlanService {
    ActivityPlan get(Long activityPlanId);
    List<ActivityPlan> listForTutorialPlan(Long tutorialPlanId);
}
