package org.uonse.numitu.server.activity.plan.rest;

import org.springframework.stereotype.Component;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;
import org.uonse.numitu.server.util.Transform;

/**
 * Created by Michael Hudson
 *
 * @since 26/08/2015.
 */
@Component
public class ActivityPlanDtoTransformer implements Transform<ActivityPlan, ActivityPlanDto>{
    @Override
    public ActivityPlanDto transform(ActivityPlan activityPlan) {
        return new ActivityPlanDto(activityPlan.getId(), activityPlan.getTutorialPlan().getId());
    }
}
