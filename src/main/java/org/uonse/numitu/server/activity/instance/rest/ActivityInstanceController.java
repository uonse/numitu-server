package org.uonse.numitu.server.activity.instance.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.uonse.numitu.server.activity.instance.service.ActivityInstanceService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
@RestController
@Transactional
public class ActivityInstanceController {
    private ActivityInstanceService activityInstanceService;
    private ActivityInstanceDtoTransformer activityInstanceDtoTransformer;

    @Autowired
    public ActivityInstanceController(ActivityInstanceService activityInstanceService, ActivityInstanceDtoTransformer activityInstanceDtoTransformer) {
        this.activityInstanceService = activityInstanceService;
        this.activityInstanceDtoTransformer = activityInstanceDtoTransformer;
    }

    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}/tutorialsessions/{tutorialSessionId}/activities", method = RequestMethod.GET)
    public List<ActivityInstanceDto> listForTutorialSession(@PathVariable Long tutorialSessionId) {
        return activityInstanceService.listForTutorialSession(tutorialSessionId).stream().map(activityInstanceDtoTransformer::transform).collect(Collectors.toList());
    }


    @RequestMapping(value = "/tutorialplans/{tutorialPlanId}/tutorialsessions/{tutorialSessionId}/activities/{activityInstanceId}", method = RequestMethod.GET)
    public ActivityInstanceDto get(@PathVariable Long activityInstanceId) {
        return activityInstanceDtoTransformer.transform(activityInstanceService.get(activityInstanceId));
    }
}
