package org.uonse.numitu.server.activity.plan.domain;


import org.hibernate.annotations.Where;
import org.uonse.numitu.server.AbstractEntity;
import org.uonse.numitu.server.tutorial.plan.domain.TutorialPlan;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Created by mhudson on 11/08/15.
 */
@Entity
@Table(name = "activities")
@Where(clause = "activity_plan_id is null")
public class ActivityPlan extends AbstractEntity {
    @OneToOne
    @JoinColumn(name = "tutorial_id")
    private TutorialPlan tutorialPlan;

    public TutorialPlan getTutorialPlan() {
        return tutorialPlan;
    }

    public void setTutorialPlan(TutorialPlan tutorialPlan) {
        this.tutorialPlan = tutorialPlan;
    }
}
