package org.uonse.numitu.server.activity.instance.data;

import org.uonse.numitu.server.NumituRepository;
import org.uonse.numitu.server.activity.instance.domain.ActivityInstance;

/**
 * Created by mhudson
 *
 * @since 25/08/15.
 */
public interface ActivityInstanceDao extends NumituRepository<ActivityInstance> {
}
