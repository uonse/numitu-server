package org.uonse.numitu.server.activity.plan.data;

import org.uonse.numitu.server.NumituRepository;
import org.uonse.numitu.server.activity.plan.domain.ActivityPlan;

/**
 * Created by mhudson
 *
 * @since 18/08/15.
 */
public interface ActivityPlanDao extends NumituRepository<ActivityPlan>{
}
