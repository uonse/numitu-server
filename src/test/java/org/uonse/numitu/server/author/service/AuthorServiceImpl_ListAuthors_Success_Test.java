package org.uonse.numitu.server.author.service;

import org.junit.Assert;
import org.mockito.Mockito;
import org.uonse.numitu.server.author.domain.Author;
import org.uonse.numitu.server.AbstractUnitTest;
import org.uonse.numitu.server.author.data.AuthorDao;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Michael Hudson
 *
 * @since 15/05/2015.
 */
public class AuthorServiceImpl_ListAuthors_Success_Test extends AbstractUnitTest{

    //Set up all of the test variables here. These are values that the test will not change, and later on when
    // we are making sure our action works as expected these constants will make our Assert statements easier to read.
    private static final List<Author> EXPECTED_AUTHORS = Arrays.asList(new Author("Michael Hudson", "Java Daddy"),
            new Author("Aldous Huxley", "Text Daddy"),
            new Author("Dr. Seuss", "Medical Doctor"));

    //These are working variables for the test. Anything the test can change or anything that will require mocking
    // should be declared here.
    private AuthorServiceImpl authorService;
    private List<Author> actualAuthors;

    @Override
    public void setup() {
        AuthorDao mockAuthorDao = Mockito.mock(AuthorDao.class);
        Mockito.when(mockAuthorDao.findAll()).thenReturn(EXPECTED_AUTHORS);
        authorService = new AuthorServiceImpl(mockAuthorDao);
    }

    @Override
    public void action() {
        //Really shouldn't be testing this method since all it does is pass through its parameters,
        // this is really just to show how to set up tests.
        actualAuthors = authorService.listAuthors();
    }

    @Override
    public void verify() {
        Assert.assertArrayEquals(EXPECTED_AUTHORS.toArray(), actualAuthors.toArray());
    }
}
